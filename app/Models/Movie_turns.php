<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Movie_turns
 * @package App\Models
 * @version August 16, 2020, 6:09 am UTC
 *
 * @property \App\Models\movies $movie
 * @property \App\Models\turns $turn
 * @property integer $movie_id
 * @property integer $turn_id
 */
class Movie_turns extends Model
{
    use SoftDeletes;

    public $table = 'movie_turns';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'movie_id',
        'turn_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'movie_id' => 'integer',
        'turn_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'movie_id' => 'required',
        'turn_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function movie()
    {
        return $this->belongsTo(\App\Models\movie::class, 'movie_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function turn()
    {
        return $this->belongsTo(\App\Models\turn::class, 'turn_id', 'id');
    }
}
