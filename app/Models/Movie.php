<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Movie
 * @package App\Models
 * @version August 16, 2020, 6:01 am UTC
 *
 * @property string $name
 * @property boolean $status
 */
class Movie extends Model
{
    use SoftDeletes;

    public $table = 'movies';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'date',
        'image',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'date' => 'date',
        'image' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|unique:movies,name',
        'date' => 'required',
        'image' => 'required|mimes:jpg,png|max:2024',
        'status' => 'required'
    ];

    public function turn()
    {
        return $this->belongsToMany('App\Models\Turn')->withTimesTamps();
    }
}
