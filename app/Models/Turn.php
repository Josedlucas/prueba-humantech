<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Turn
 * @package App\Models
 * @version August 16, 2020, 6:07 am UTC
 *
 * @property string $turns
 * @property boolean $status
 */
class Turn extends Model
{
    use SoftDeletes;

    public $table = 'turns';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'turns',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'turns' => 'required',
        'status' => 'required'
    ];

    public function movie()
    {
        return $this->belongsToMany('App\Models\Movie')->withTimesTamps();
    }
    
}
