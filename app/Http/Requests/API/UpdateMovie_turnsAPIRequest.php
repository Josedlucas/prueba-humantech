<?php

namespace App\Http\Requests\API;

use App\Models\Movie_turns;
use InfyOm\Generator\Request\APIRequest;

class UpdateMovie_turnsAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Movie_turns::$rules;
        
        return $rules;
    }
}
