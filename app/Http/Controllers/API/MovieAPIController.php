<?php

namespace App\Http\Controllers\API;


use App\Models\Turn;
use App\Models\Movie;
use App\Repositories\MovieRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Validation\ValidationException;
use Response;

/**
 * Class MovieController
 * @package App\Http\Controllers\API
 */

class MovieAPIController extends AppBaseController
{
    /** @var  MovieRepository */
    private $movieRepository;

    public function __construct(MovieRepository $movieRepo)
    {
        $this->movieRepository = $movieRepo;
    }

    /**
     * Display a listing of the Movie.
     * GET|HEAD /movies
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $movies = $this->movieRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($movies->toArray(), 'Consulta exitosa');
    }

    /**
     * Store a newly created Movie in storage.
     * POST /movies
     *
     * @param CreateMovieAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request, Movie $movie)
    {
        
        try {
            $this->validate($request, [
                'name' => 'required|unique:movies,name',
                'date' => 'required',
                'image' => 'required|mimes:jpg,png|max:2024',
                'status' => 'required'
            ],
            [
                'name.required' => 'El campo nombre es requerido',
                'name.unique' => 'El nombre debe ser unico',
                'date.required' => 'El campo date es requerido',
                'imagen.mimes' => 'La imagen debe ser jpg o png',
                'status.required' => 'El status debe es requerido',
            ]);
        } catch (ValidationException $e) {
            return response()->json($e->validator->errors(), 422);
        }
 
        $input = $request->all();

        if ($files = $request->file('image')) {
            $destinationPath = 'uploads/';
            $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $fileName);
            $input['image'] = "$fileName";
        }
        
        $movie = $this->movieRepository->create($input);

        if ($request->get('turns_id')) {
            $movie->turn()->sync($request->get('turns_id'));
        }

        return $this->sendResponse($movie->toArray(), 'Pelicula guardada con exito');
    }

  
    /**
     * Display the specified Movie.
     * GET|HEAD /movies/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Movie $movie */
        $movie = $this->movieRepository->find($id);

        if (empty($movie)) {
            return $this->sendError('Movie not found');
        }

        return $this->sendResponse($movie->toArray(), 'Consulta exitosa');
    }

    /**
     * Update the specified Movie in storage.
     * PUT/PATCH /movies/{id}
     *
     * @param int $id
     * @param UpdateMovieAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {   
        try {
            $this->validate($request, [
                'name' => "required|unique:movies,name,".$id,
                'date' => 'required',
                'image' => 'required|mimes:jpg,png|max:2024',
                'status' => 'required'
            ],
            [
                'name.required' => 'El campo nombre es requerido',
                'name.unique' => 'El nombre debe ser unico',
                'date.required' => 'El campo date es requerido',
                'imagen.mimes' => 'La imagen debe ser jpg o png',
                'status.required' => 'El status debe es requerido',
            ]);
        } catch (ValidationException $e) {
            return response()->json($e->validator->errors(), 422);
        }
        
        $input = $request->all();

        if ($files = $request->file('image')) {
            $destinationPath = 'uploads/';
            $fileName = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $fileName);
            $input['image'] = "$fileName";
        }
        if ($request->get('turns_id')) {
            $movie->turn()->sync($request->get('turns_id'));
        }

        /** @var Movie $movie */
        $movie = $this->movieRepository->find($id);

        if (empty($movie)) {
            return $this->sendError('Movie not found');
        }

        $movie = $this->movieRepository->update($input, $id);

        return $this->sendResponse($movie->toArray(), 'Actualizacion exitosa');
    }

    /**
     * Remove the specified Movie from storage.
     * DELETE /movies/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Movie $movie */
        $movie = $this->movieRepository->find($id);

        if (empty($movie)) {
            return $this->sendError('Movie not found');
        }

        $movie->delete();

        return $this->sendSuccess('Registro eliminado con exito');
    }
}
