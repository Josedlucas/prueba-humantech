<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTurnAPIRequest;
use App\Http\Requests\API\UpdateTurnAPIRequest;
use App\Models\Turn;
use App\Repositories\TurnRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TurnController
 * @package App\Http\Controllers\API
 */

class TurnAPIController extends AppBaseController
{
    /** @var  TurnRepository */
    private $turnRepository;

    public function __construct(TurnRepository $turnRepo)
    {
        $this->turnRepository = $turnRepo;
    }

    /**
     * Display a listing of the Turn.
     * GET|HEAD /turns
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $turns = $this->turnRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($turns->toArray(), 'Consulta exitosa');
    }

    /**
     * Store a newly created Turn in storage.
     * POST /turns
     *
     * @param CreateTurnAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'turns' => 'required',
                'status' => 'required'
            ],
            [
                'turns.required' => 'El campo turns es requerido',
                'status.required' => 'El status debe es requerido',
            ]);
        } catch (ValidationException $e) {
            return response()->json($e->validator->errors(), 422);
        }
 
        $input = $request->all();

        $turn = $this->turnRepository->create($input);

        return $this->sendResponse($turn->toArray(), 'Turno guardada con exito');
    }

    /**
     * Display the specified Turn.
     * GET|HEAD /turns/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Turn $turn */
        $turn = $this->turnRepository->find($id);

        if (empty($turn)) {
            return $this->sendError('Turn not found');
        }

        return $this->sendResponse($turn->toArray(), 'Consulta exitosa');
    }

    /**
     * Update the specified Turn in storage.
     * PUT/PATCH /turns/{id}
     *
     * @param int $id
     * @param UpdateTurnAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTurnAPIRequest $request)
    {   
        try {
            $this->validate($request, [
                'turns' => 'required',
                'status' => 'required'
            ],
            [
                'turns.required' => 'El campo turns es requerido',
                'status.required' => 'El status debe es requerido',
            ]);
        } catch (ValidationException $e) {
            return response()->json($e->validator->errors(), 422);
        }

        $input = $request->all();

        /** @var Turn $turn */
        $turn = $this->turnRepository->find($id);

        if (empty($turn)) {
            return $this->sendError('Turn not found');
        }

        $turn = $this->turnRepository->update($input, $id);

        return $this->sendResponse($turn->toArray(), 'Actualizacion exitosa');
    }

    /**
     * Remove the specified Turn from storage.
     * DELETE /turns/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Turn $turn */
        $turn = $this->turnRepository->find($id);

        if (empty($turn)) {
            return $this->sendError('Turn not found');
        }

        $turn->delete();

        return $this->sendSuccess('Registro eliminado con exito');
    }
}
