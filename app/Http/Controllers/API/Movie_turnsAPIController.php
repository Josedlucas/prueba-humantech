<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMovie_turnsAPIRequest;
use App\Http\Requests\API\UpdateMovie_turnsAPIRequest;
use App\Models\Movie_turns;
use App\Repositories\Movie_turnsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Movie_turnsController
 * @package App\Http\Controllers\API
 */

class Movie_turnsAPIController extends AppBaseController
{
    /** @var  Movie_turnsRepository */
    private $movieTurnsRepository;

    public function __construct(Movie_turnsRepository $movieTurnsRepo)
    {
        $this->movieTurnsRepository = $movieTurnsRepo;
    }

    /**
     * Display a listing of the Movie_turns.
     * GET|HEAD /movieTurns
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $movieTurns = $this->movieTurnsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($movieTurns->toArray(), 'Movie Turns retrieved successfully');
    }

    /**
     * Store a newly created Movie_turns in storage.
     * POST /movieTurns
     *
     * @param CreateMovie_turnsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMovie_turnsAPIRequest $request)
    {
        $input = $request->all();

        $movieTurns = $this->movieTurnsRepository->create($input);

        return $this->sendResponse($movieTurns->toArray(), 'Movie Turns saved successfully');
    }

    /**
     * Display the specified Movie_turns.
     * GET|HEAD /movieTurns/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Movie_turns $movieTurns */
        $movieTurns = $this->movieTurnsRepository->find($id);

        if (empty($movieTurns)) {
            return $this->sendError('Movie Turns not found');
        }

        return $this->sendResponse($movieTurns->toArray(), 'Movie Turns retrieved successfully');
    }

    /**
     * Update the specified Movie_turns in storage.
     * PUT/PATCH /movieTurns/{id}
     *
     * @param int $id
     * @param UpdateMovie_turnsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMovie_turnsAPIRequest $request)
    {
        $input = $request->all();

        /** @var Movie_turns $movieTurns */
        $movieTurns = $this->movieTurnsRepository->find($id);

        if (empty($movieTurns)) {
            return $this->sendError('Movie Turns not found');
        }

        $movieTurns = $this->movieTurnsRepository->update($input, $id);

        return $this->sendResponse($movieTurns->toArray(), 'Movie_turns updated successfully');
    }

    /**
     * Remove the specified Movie_turns from storage.
     * DELETE /movieTurns/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Movie_turns $movieTurns */
        $movieTurns = $this->movieTurnsRepository->find($id);

        if (empty($movieTurns)) {
            return $this->sendError('Movie Turns not found');
        }

        $movieTurns->delete();

        return $this->sendSuccess('Movie Turns deleted successfully');
    }
}
