<?php

namespace App\Repositories;

use App\Models\Movie_turns;
use App\Repositories\BaseRepository;

/**
 * Class Movie_turnsRepository
 * @package App\Repositories
 * @version August 16, 2020, 6:09 am UTC
*/

class Movie_turnsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'movie_id',
        'turn_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Movie_turns::class;
    }
}
