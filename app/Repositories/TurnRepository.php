<?php

namespace App\Repositories;

use App\Models\Turn;
use App\Repositories\BaseRepository;

/**
 * Class TurnRepository
 * @package App\Repositories
 * @version August 16, 2020, 6:07 am UTC
*/

class TurnRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'turns',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Turn::class;
    }
}
