<?php

namespace App\Repositories;

use App\Models\Movie;
use App\Repositories\BaseRepository;

/**
 * Class MovieRepository
 * @package App\Repositories
 * @version August 16, 2020, 6:01 am UTC
*/

class MovieRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Movie::class;
    }
}
