<?php namespace Tests\Repositories;

use App\Models\Movie_turns;
use App\Repositories\Movie_turnsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Movie_turnsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Movie_turnsRepository
     */
    protected $movieTurnsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->movieTurnsRepo = \App::make(Movie_turnsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_movie_turns()
    {
        $movieTurns = factory(Movie_turns::class)->make()->toArray();

        $createdMovie_turns = $this->movieTurnsRepo->create($movieTurns);

        $createdMovie_turns = $createdMovie_turns->toArray();
        $this->assertArrayHasKey('id', $createdMovie_turns);
        $this->assertNotNull($createdMovie_turns['id'], 'Created Movie_turns must have id specified');
        $this->assertNotNull(Movie_turns::find($createdMovie_turns['id']), 'Movie_turns with given id must be in DB');
        $this->assertModelData($movieTurns, $createdMovie_turns);
    }

    /**
     * @test read
     */
    public function test_read_movie_turns()
    {
        $movieTurns = factory(Movie_turns::class)->create();

        $dbMovie_turns = $this->movieTurnsRepo->find($movieTurns->id);

        $dbMovie_turns = $dbMovie_turns->toArray();
        $this->assertModelData($movieTurns->toArray(), $dbMovie_turns);
    }

    /**
     * @test update
     */
    public function test_update_movie_turns()
    {
        $movieTurns = factory(Movie_turns::class)->create();
        $fakeMovie_turns = factory(Movie_turns::class)->make()->toArray();

        $updatedMovie_turns = $this->movieTurnsRepo->update($fakeMovie_turns, $movieTurns->id);

        $this->assertModelData($fakeMovie_turns, $updatedMovie_turns->toArray());
        $dbMovie_turns = $this->movieTurnsRepo->find($movieTurns->id);
        $this->assertModelData($fakeMovie_turns, $dbMovie_turns->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_movie_turns()
    {
        $movieTurns = factory(Movie_turns::class)->create();

        $resp = $this->movieTurnsRepo->delete($movieTurns->id);

        $this->assertTrue($resp);
        $this->assertNull(Movie_turns::find($movieTurns->id), 'Movie_turns should not exist in DB');
    }
}
