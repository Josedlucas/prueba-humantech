<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Movie_turns;

class Movie_turnsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_movie_turns()
    {
        $movieTurns = factory(Movie_turns::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/movie_turns', $movieTurns
        );

        $this->assertApiResponse($movieTurns);
    }

    /**
     * @test
     */
    public function test_read_movie_turns()
    {
        $movieTurns = factory(Movie_turns::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/movie_turns/'.$movieTurns->id
        );

        $this->assertApiResponse($movieTurns->toArray());
    }

    /**
     * @test
     */
    public function test_update_movie_turns()
    {
        $movieTurns = factory(Movie_turns::class)->create();
        $editedMovie_turns = factory(Movie_turns::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/movie_turns/'.$movieTurns->id,
            $editedMovie_turns
        );

        $this->assertApiResponse($editedMovie_turns);
    }

    /**
     * @test
     */
    public function test_delete_movie_turns()
    {
        $movieTurns = factory(Movie_turns::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/movie_turns/'.$movieTurns->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/movie_turns/'.$movieTurns->id
        );

        $this->response->assertStatus(404);
    }
}
