<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Turn;

class TurnApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_turn()
    {
        $turn = factory(Turn::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/turns', $turn
        );

        $this->assertApiResponse($turn);
    }

    /**
     * @test
     */
    public function test_read_turn()
    {
        $turn = factory(Turn::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/turns/'.$turn->id
        );

        $this->assertApiResponse($turn->toArray());
    }

    /**
     * @test
     */
    public function test_update_turn()
    {
        $turn = factory(Turn::class)->create();
        $editedTurn = factory(Turn::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/turns/'.$turn->id,
            $editedTurn
        );

        $this->assertApiResponse($editedTurn);
    }

    /**
     * @test
     */
    public function test_delete_turn()
    {
        $turn = factory(Turn::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/turns/'.$turn->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/turns/'.$turn->id
        );

        $this->response->assertStatus(404);
    }
}
