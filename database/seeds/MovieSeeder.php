<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        // DB::table('movies')->truncate();
        
        DB::table('movies')->insert([
            'name' => 'X Men: Días del futuro pasado',
            'date' => '2020-08-16',
            'image' => 'none',
            'status' => 1,
            'created_at' => Carbon\Carbon::now(),
        ]);
        DB::table('movies')->insert([
            'name' => 'Alicia en el país de la maravillas',
            'date' => '2020-08-16',
            'image' => 'none',
            'status' => 1,
            'created_at' => Carbon\Carbon::now(),
        ]);
        DB::table('movies')->insert([
            'name' => 'Locos de amor',
            'date' => '2020-08-16',
            'image' => 'none',
            'status' => 1,
            'created_at' => Carbon\Carbon::now(),
        ]);
        DB::table('movies')->insert([
            'name' => 'Tortugas Ninja 2',
            'date' => '2020-08-16',
            'image' => 'none',
            'status' => 1,
            'created_at' => Carbon\Carbon::now(),
        ]);
    }
}
