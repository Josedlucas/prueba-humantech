<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class TurnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('turns')->truncate();
        
        DB::table('turns')->insert([
            'turns' => '10:30',
            'status' => 1,
            'created_at' => Carbon\Carbon::now(),
        ]);
        DB::table('turns')->insert([
            'turns' => '12:30',
            'status' => 1,
            'created_at' => Carbon\Carbon::now(),
        ]);
        DB::table('turns')->insert([
            'turns' => '01:30',
            'status' => 1,
            'created_at' => Carbon\Carbon::now(),
        ]);
        DB::table('turns')->insert([
            'turns' => '02:30',
            'status' => 1,
            'created_at' => Carbon\Carbon::now(),
        ]);
        DB::table('turns')->insert([
            'turns' => '03:30',
            'status' => 1,
            'created_at' => Carbon\Carbon::now(),
        ]);
    }
}
