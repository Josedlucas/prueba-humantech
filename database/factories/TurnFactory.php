<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Turn;
use Faker\Generator as Faker;

$factory->define(Turn::class, function (Faker $faker) {

    return [
        'turns' => $faker->date('Y-m-d H:i:s'),
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
