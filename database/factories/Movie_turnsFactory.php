<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Movie_turns;
use Faker\Generator as Faker;

$factory->define(Movie_turns::class, function (Faker $faker) {

    return [
        'movie_id' => $faker->randomDigitNotNull,
        'turn_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
