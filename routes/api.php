<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/movies', 'MovieAPIController@store');
Route::get('/movies', 'MovieAPIController@index');
Route::delete('/movies/{id}', 'MovieAPIController@destroy');
Route::get('/movies/{id}', 'MovieAPIController@show');
Route::post('/movies/{id}', 'MovieAPIController@update');


Route::post('/turns', 'TurnAPIController@store');
Route::get('/turns', 'TurnAPIController@index');
Route::delete('/turns/{id}', 'TurnAPIController@destroy');
Route::get('/turns/{id}', 'TurnAPIController@show');
Route::post('/turns/{id}', 'TurnAPIController@update');
